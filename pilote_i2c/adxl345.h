#ifndef ADXL345_H
#define ADXL345_H

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/i2c.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/ioctl.h>
#include <linux/kfifo.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>


/////////////////////////
// REGISTERS ADDRESSES //  
/////////////////////////

#define DEVID               0x0

/* 0x01 to 0x1C are reserved */

#define THRESH_TAP          0x1D
#define OFSX                0x1E
#define OFSY                0x1F
#define OFSZ                0x20
#define DUR                 0x21
#define LATENT              0x22
#define WINDOW              0x23
#define THRESH_ACT          0x24
#define THRESH_INACT        0x25
#define TIME_INACT          0x26
#define ACT_INACT_CTL       0x27
#define THRESH_FF           0x28
#define TIME_FF             0x29
#define TAP_AXES            0x2A
#define ACT_TAP_STATUS      0x2B
#define BW_RATE             0x2C
#define POWER_CTL           0x2D
#define INT_ENABLE          0x2E
#define INT_MAP             0x2F
#define INT_SOURCE          0x30
#define DATA_FORMAT         0x31
#define DATAX0              0x32
#define DATAX1              0x33
#define DATAY0              0x34
#define DATAY1              0x35
#define DATAZ0              0x36
#define DATAZ1              0x37
#define FIFO_CTL            0x38
#define FIFO_STATUS         0x39

///////////////
// POWER CTL //
///////////////

#define LINK                (1<<5)
#define AUTO_SLEEP          (1<<4)
#define MEASURE             (1<<3)
#define SLEEP               (1<<2)
#define WAKEUP_MASK         0b11
    
////////////////
// INTERRUPTS //
////////////////

#define DATA_READY          (1<<7)
#define SINGLE_TAP          (1<<6)
#define DOUBLE_TAP          (1<<5)
#define ACTIVITY            (1<<4)
#define INACTIVITY          (1<<3)
#define FREE_FALL           (1<<2)
#define WATERMARK           (1<<1)
#define OVERRUN             1

///////////////
// FIFO MODE //
///////////////

#define FIFO_MODE_OFFSET    6
#define TRIGGER_OFFSET      5
#define SAMPLES_MASK        0b11111
#define BYPASS              0b00
#define FIFO                0b01
#define STREAM              0b10
#define TRIGGER             0b11

#define FIFO_ENTRIES_MASK   0b00111111

#define FIFO_SIZE           64

/////////////////////////
// SPECIFIC PARAMETERS //
/////////////////////////

#define FREQ_100            0x0A
#define WATERMARK_LIMIT     20

//////////////////////////////
// DIRECTION CHOICE COMMAND //
//////////////////////////////

/* Free type for command register with ioctl */
#define CMD_BASE            0x91

/* Define 3 commands with to macro IO, as there is no argument for these commands */
#define X_AXIS              0
#define Y_AXIS              1
#define Z_AXIS              2
#define FULL_SAMPLE         3

#define SET_X_AXIS          _IO(CMD_BASE, X_AXIS)
#define SET_Y_AXIS          _IO(CMD_BASE, Y_AXIS)
#define SET_Z_AXIS          _IO(CMD_BASE, Z_AXIS)
#define SET_FULL            _IO(CMD_BASE, FULL_SAMPLE)


///////////////////////
// TYPE DECLARATIONS //
///////////////////////

/* Driver FIFO element 
 Stored data has to be unsigned to avoid any sign interpretation during casting */
struct fifo_element {
    uint8_t x[2];
    uint8_t y[2];
    uint8_t z[2];
};

/* To represent an adxl_345 device */
struct adxl345_device {

    /* Misc device structure, for easy registering */
    struct miscdevice miscdev;

    /* To store the current read axis */
    /* /!\ Hardware read axis is configured as a hardware parameter
            In a better version, it should be considered as a reading option
            (in the struct file for example)
            Here, we can NOT assure that after selecting an axis with an IOCTL
            command, another application would not modify it before reading
            Therefore, there is no guarantee that the read axis is the requested one
            from now.
    */
    atomic_t axis;

    /* Waiting queue when no data is available */
    wait_queue_head_t * queue;

    /* Software FIFO to store read values and associated spinlock */
    DECLARE_KFIFO(samples_fifo, struct fifo_element, FIFO_SIZE);
    spinlock_t fifo_s ;

    /* Basic method to guarantee atomic access to hardware */
    struct mutex hw_access_m ;
};


////////////////////////////
// FUNCTIONS DECLARATIONS //
////////////////////////////

/* Reading function (from user space) */
ssize_t adxl345_read( struct file *, char __user *, size_t , loff_t *);

/* IRQ handler */
irqreturn_t adxl345_int(int , void *);

/* IOCTL function to set read axis*/
long adxl345_unlock_ioctl(struct file * , unsigned int , unsigned long);

/* Read count bytes in the internal register at register_address of the ADXL345 device and place them in count
/!\ Warning: This is assumed the buff is large enough to contain the data
*/
static int adxl345_read_bytes(struct i2c_client *, uint8_t , uint8_t *, int );

/* Write count bytes at the internal register at register_address of the ADXL345 device from the buffer buff
/!\ Warning: It is assumed that the buff containss enough data and will not cause segmentation fault
*/
// static int adxl345_write_bytes(struct i2c_client *, uint8_t , uint8_t * , int );

/* Read value from one register at register_adress and returns it and store it at the provided adress*/
static int adxl345_read_from_reg(struct i2c_client *, uint8_t , uint8_t * );

/* Write one register at register_adress with the provided value */
static int adxl345_write_to_reg(struct i2c_client *, uint8_t , uint8_t );

/* Read data provided by the accelerometer and stores it in result:
*   - x position: 16 bits value stored in result[0] and result[1] (result[0] contains the least significant bits)
*   - y position: 16 bits value stored in result[2] and result[3] (result[2] contains the least significant bits)
*   - z position: 16 bits value stored in result[4] and result[5] (result[4] contains the least significant bits)
*/
static int adxl345_read_data(struct i2c_client *, uint8_t *);

#endif // ADXL345_H