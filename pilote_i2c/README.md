# Embedded Linux: ADXL345 linux driver

The actual driver code is in the folder named `pilote_i2c`. The other folder `premier_module` contains the code of a basic linux module to test integration into a distribution.

## Context

This driver has been developed as a graded task for a course named "Embedded Linux" in the SETI Master's degree from Université Paris-Saclay, given at Télécom Paris.

This is a learning project, not intented to be used for real applications.

## ADXL345 accelerometer

The ADXL345 is a triple-axis accelerometer that can be connected to the I2C or the SPI bus. This is a low-cost sensor that can be integrated as a device in a Linux distribution.

Some documentation about it is available [here](https://perso.telecom-paristech.fr/duc/cours/linux/doc/ADXL345.pdf)

An official driver is also present at [this address](https://elixir.bootlin.com/linux/latest/source/drivers/input/misc/adxl34x.c)

