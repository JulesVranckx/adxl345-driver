#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include<sys/ioctl.h>

#define CMD_BASE 0x91
#define SET_X_AXIS _IO(CMD_BASE, 0)
#define SET_Y_AXIS _IO(CMD_BASE, 1)
#define SET_Z_AXIS _IO(CMD_BASE, 2)
#define SET_FULL   _IO(CMD_BASE, 3)

extern int errno;

int main(int argc, char *argv[]) {
    printf("Testing...\n");

    int error;
    uint8_t x_buff[2] ;
    int16_t x, y ,z;
    uint8_t sample[6];
    int i,j;
    volatile long long useless;

    int f = open("/dev/adxl345", O_RDONLY);

    if (f==-1){
        printf("[-] Error while opening driver, abort\n");
        exit(-1);
    }


    for(i=0; i<100; i++) {
        // for (j=0; j<300000; j++) {
        //         useless++;
        //     }
            ioctl(f, SET_X_AXIS, NULL);
            error = read(f, x_buff, 2);
            if (error <0) {
                printf("[-] Error while reading: %d\n", errno);
                exit(-1);
            }
            x = (int16_t)x_buff[0] | ((int16_t)x_buff[1] << 8);
            printf("\t#%d  x: %d\n",i,x);

            ioctl(f, SET_Y_AXIS, NULL);
            error = read(f, x_buff, 2);
            if (error <0) {
                printf("[-] Error while reading: %d\n", errno);
                exit(-1);
            }
            y = (int16_t)x_buff[0] | ((int16_t)x_buff[1] << 8);
            printf("\t#%d  y: %d\n",i, y);

            ioctl(f, SET_Z_AXIS, NULL);
            error = read(f, x_buff, 2);
            if (error <0) {
                printf("[-] Error while reading: %d\n", errno);
                exit(-1);
            }
            z = (int16_t)x_buff[0] | ((int16_t)x_buff[1] << 8);
            printf("\t#%d  z: %d\n",i, z);

            ioctl(f, SET_FULL, NULL);
            error = read(f, sample, 6);
            if (error <0) {
                printf("[-] Error while reading: %d\n", errno);
                exit(-1);
            }
            x = (int16_t)sample[0] | ((int16_t)sample[1] << 8);
            y = (int16_t)sample[2] | ((int16_t)sample[3] << 8);
            z = (int16_t)sample[4] | ((int16_t)sample[5] << 8);
            printf("\t#%d  full sample: x: %d, y:%d, z: %d\n",i, x, y, z);
            
    }
    
}