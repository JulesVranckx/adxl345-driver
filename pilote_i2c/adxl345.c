#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/of.h>
#include <linux/i2c.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/ioctl.h>
#include <linux/kfifo.h>
#include <linux/interrupt.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include "adxl345.h"

/////////////////////////
// GLOBAL  DEFINITIONS //
/////////////////////////

/* Global variable to contain the dirver operations */
struct file_operations fops = {
    .owner  = THIS_MODULE,
    .read   = adxl345_read,
    .unlocked_ioctl = adxl345_unlock_ioctl,
};

/////////////////////////////////////
// INTERNAL READ & WRITE FUNCTIONS //
/////////////////////////////////////

static int adxl345_read_bytes(struct i2c_client *client, uint8_t register_address, uint8_t *buff, int count) {

    int res;

    struct adxl345_device * this_device = i2c_get_clientdata(client);

    /* Lock for atomic reading */
    res = mutex_lock_interruptible(&this_device->hw_access_m);
    if (res == -EINTR){
        return -ERESTARTSYS ;
    }

    /* Send the register address */
    res = i2c_master_send(client, &register_address, 1);
    if (res != 1) {
        pr_err("[ADXL345][-] Error when sending reading address\n");
        return -1 ;
    }

    /* Read the data into buff */
    res = i2c_master_recv(client, buff, count);
    if (res != count) {
        pr_err("[ADXL345][-] Error when reading data\n");
    }

    /* Unlock */
    mutex_unlock(&this_device->hw_access_m);

    return res ;
}

/* This function is never used, but is supposed to be working 
   It can be used to write several register in once */
// static int adxl345_write_bytes(struct i2c_client *client, uint8_t register_address, uint8_t * buff, int count) {

//     int i;
//     int res;

//     struct adxl345_device * this_device = i2c_get_clientdata(client);

//     /* Define an internal buffer to send the address and the data in a row */
//     uint8_t *internal_buff = (uint8_t * )kzalloc(sizeof(uint8_t) * (count + 1), GFP_KERNEL);
//     if (internal_buff == NULL) {
//         pr_err("[ADXL345][-] Memory allocation failed\n");
//         return -1;
//     }
//     internal_buff[0] = register_address ;
//     for (i=1; i<=count; i++) {
//         internal_buff[i] = buff[i-1];
//     }

//     /* Lock for atomic reading */
//     res = mutex_lock_interruptible(&this_device->hw_access_m);
//     if (res == -EINTR){
//         return -ERESTARTSYS ;
//     }

//     /* Send the new buffer */
//     res = i2c_master_send(client, internal_buff, count+1);
//     if (res != count + 1) {
//         pr_err("[ADXL345][-] Error when sending data\n");
//     }

//     /* Unlock */
//     mutex_unlock(&this_device->hw_access_m);

//     /* Free the allocated memory */
//     kfree(internal_buff);

//     return res;
// }

static int adxl345_read_from_reg(struct i2c_client *client, uint8_t register_address, uint8_t * ret) {
    return adxl345_read_bytes(client, register_address, ret, 1);
}

static int adxl345_write_to_reg(struct i2c_client *client, uint8_t register_address, uint8_t data){
    
    // this version uses a kzalloc, but this is not compulsory here
    // We thus define a more efficient method to write a single byte
    // uint8_t tmp_buff = data ;
    // return adxl345_write_bytes(client, register_address, &tmp_buff, 1);

    /* Concatenate address and data in a buffer */
    uint8_t buff[2] = {register_address, data} ;
    int res ;

    struct adxl345_device * this_device = i2c_get_clientdata(client);

    /* Lock for atomic reading */
    res = mutex_lock_interruptible(&this_device->hw_access_m);
    if (res == -EINTR){
        return -ERESTARTSYS ;
    }
    
    res = i2c_master_send(client, buff, 2);
    if (res != 2) {
        pr_err("[ADXL345][-] Error when sending data\n");
    }

    /* Unlock */
    mutex_unlock(&this_device->hw_access_m);

    return res;


}

static int adxl345_read_data(struct i2c_client *client, uint8_t *result){
    int res =  adxl345_read_bytes(client, DATAX0, result, 6);
    return res;
}

////////////////////
// USER FUNCTIONS //
////////////////////

ssize_t adxl345_read( struct file * file , char __user * buf ,size_t count , loff_t * f_pos ) {

    /* Declarations */
    int res;
    struct fifo_element xyz;
    uint8_t *buff_kernel;
    uint8_t sample[6];
    unsigned long flags;
    int crrt_axis;

    /* Get the adxl345 device struct */
    struct miscdevice *miscdev = file->private_data ;
    struct adxl345_device *this_device = (struct adxl345_device *)container_of(miscdev, struct adxl345_device, miscdev);
    

    // Check correctness of arguments //
    
    /* Avoid null pointer writing */
    if (!buf){
        pr_err("[ADXL345][-] Can't read into NULL buffer, aborting\n");
        return -1;
    }

    /* If read of 0 byte requested, do nothing */
    if (!count){
        return 0;
    }

    /* If read only one axis, set count to 1 or 2*/
    if (count > 2 && crrt_axis != FULL_SAMPLE) {
        count = 2;
    }

    /* Store current axis */
    crrt_axis = atomic_read(&this_device->axis);

    
    
    spin_lock_irqsave(&this_device->fifo_s, flags);

    while(kfifo_is_empty(&this_device->samples_fifo)) {
        spin_unlock_irqrestore(&this_device->fifo_s, flags);

        /* Wait until FIFO is not empty */
        res = wait_event_interruptible(*this_device->queue, !kfifo_is_empty(&this_device->samples_fifo));
        if (res == -ERESTARTSYS) {
            return res;
        }

        spin_lock_irqsave(&this_device->fifo_s, flags); 

    }

    /* Read from FIFO */
    res = kfifo_get(&this_device->samples_fifo, &xyz); 

    /* Lock the mutex */
    spin_unlock_irqrestore(&this_device->fifo_s, flags);

    /* Set the appropriate kernel to copy from*/
    switch(crrt_axis) {
        case X_AXIS:
            buff_kernel = xyz.x ;
            break;

        case Y_AXIS:
            buff_kernel = xyz.y ;
            break;

        case Z_AXIS:
            buff_kernel = xyz.z ;
            break;

        case FULL_SAMPLE:
            count = 6 ;
            memcpy(sample, xyz.x, count);
            buff_kernel = sample;
            break;
    }

    /* Copy the data */
    res = copy_to_user(buf, buff_kernel, count);
    if (res){
        pr_alert("[ADXL345][-] Failed to copy all data from ADXL345 device\n");
    }

    return (count - res);
}

long adxl345_unlock_ioctl(struct file * file ,unsigned int cmd ,unsigned long arg) {

    /* Get the i2c_client struct */
    struct miscdevice *miscdev = file->private_data ;
    struct adxl345_device *this_device = (struct adxl345_device *)container_of(miscdev, struct adxl345_device, miscdev);

    switch (cmd) {

        case SET_X_AXIS:
            atomic_set(&this_device->axis, X_AXIS) ;
            break ;

        case SET_Y_AXIS:
            atomic_set(&this_device->axis, Y_AXIS) ;
            break ;

        case SET_Z_AXIS:
            atomic_set(&this_device->axis, Z_AXIS) ;
            break ;

        case SET_FULL:
            atomic_set(&this_device->axis, FULL_SAMPLE);
            break ;

        default: 
            return -ENOIOCTLCMD;
    }

    return 0;
}

//////////////////////////
// Interruption handler //
//////////////////////////

irqreturn_t adxl345_int(int irq, void *dev_id) {

    /* Declarations */
    uint8_t n_samples;
    int res ;
    int i;
    uint8_t raw_data[6];
    struct fifo_element el;
    struct adxl345_device * this_device = (struct adxl345_device *)dev_id ;
    struct device * dev = this_device->miscdev.parent;
    struct i2c_client *client = to_i2c_client(dev);

    /* Read number of values */
    res = adxl345_read_from_reg(client, FIFO_STATUS, &n_samples);
    if (res != 1) {
        pr_alert("[ADXL345][-] Can't read number of samples in the FIFO, aborting\n");
        return IRQ_HANDLED; // hardware FIFO could overflow, but IRQ is done being handling
    }
    n_samples = n_samples & FIFO_ENTRIES_MASK ;

    for (i=0; i<n_samples; i++) {
        /* Read from the hardware FIFO and store in the driver FIFO */
        res = adxl345_read_data(client, raw_data);
        if (res != 6){
            pr_err("[ADXL345][-] Can't read full sample from FIFO (read only %d), aborting reading\n", res);
            continue;
        }
        memcpy(el.x, raw_data, 6);

        /* Protected FIFO writing 
        * /!\ No problem to block here, as we are in the bottom-half function executed in a dedicated
        * kernel thread, using the appropriate spinlock mechanisms
        */
        spin_lock(&this_device->fifo_s);
        res = kfifo_put(&this_device->samples_fifo, el);
        spin_unlock(&this_device->fifo_s);

        if (res != 1){
            /* Following line pollutes command line */
            // pr_alert("[ADXL345][-] Software FIFO full\n");
            continue;
        }
    }

    /* Wake up sleepy tasks */
    wake_up_interruptible(this_device->queue);

    return IRQ_HANDLED;
}

////////////////////////////////
// I2C DRIVER BASIC FUNCTIONS //
////////////////////////////////

static int adxl345_probe(struct i2c_client *client,
                     const struct i2c_device_id *id)
{
    /* Declarations */
    uint8_t DEVID_buff = 0;
    int nbr_iter;
    volatile int useless = 0 ;
    long long i;
    uint8_t xyz[6] = {0};
    int16_t x,y,z;
    uint8_t fifo_mode;
    int irq_register_ret;
    struct adxl345_device *this_device;


    /* Notify the function is called */
    pr_info("[ADXL345][+] I2C Module loaded\n");

    /* Allocate memory for the device struct and fill it */
    this_device = (struct adxl345_device *)kzalloc(sizeof(struct adxl345_device), GFP_KERNEL);
    if (this_device == NULL){
        pr_err("[ADXL345][-] Device struct malloc failed\n");
        return -1 ;
    }
    atomic_set(&this_device->axis, X_AXIS);
    this_device->miscdev.minor = MISC_DYNAMIC_MINOR ;
    this_device->miscdev.name = "adxl345";
    this_device->miscdev.fops = &fops ;

    /* Initalize FIFO */
    INIT_KFIFO(this_device->samples_fifo);
    pr_info("[ADXL345][+] FIFO initialized\n");

    /* Initialize queue */
    this_device->queue = (wait_queue_head_t *)kzalloc(sizeof(wait_queue_head_t), GFP_KERNEL);
    init_waitqueue_head(this_device->queue);
    pr_info("[ADXL345][+] Wait queue initialized\n");

    /* Initialize mutex and spinlock */
    spin_lock_init(&this_device->fifo_s);
    mutex_init(&this_device->hw_access_m);

    /* Connect the client and the device */
    this_device->miscdev.parent = &client->dev; 
    i2c_set_clientdata(client, this_device);
    pr_info("[ADXL345][+] I2C client and device connected\n");

    /* Register IRQ handler */
    irq_register_ret = devm_request_threaded_irq(&client->dev, client->irq, NULL, adxl345_int, IRQF_ONESHOT, "adxl345", this_device);
    if (irq_register_ret < 0) {
        pr_err("[ADXL345][-] IRQ register failed, abort loading module...\n");
        kfree(this_device);
        return -1;
    }
    pr_info("[ADXL345][+] IRQ handler activated\n");
    
    /* Read the DEVID register and display its value */
    adxl345_read_from_reg(client, DEVID, &DEVID_buff);
    pr_info("[ADXL345][+] Read DEVID value for checking: %hhx\n", DEVID_buff);

    /* Configuration of the output data rate (100Hz) */
    adxl345_write_to_reg(client, BW_RATE, FREQ_100);
    pr_info("[ADXL345][+] Output data rate set to 100Hz\n");

    /* Disable all interruption except watermark one*/
    adxl345_write_to_reg(client, INT_ENABLE, (uint8_t) WATERMARK);
    pr_info("[ADXL345][+] All interruptions disabled except watermal one\n");

    /* Set default data format */
    adxl345_write_to_reg(client, DATA_FORMAT, 0);
    pr_info("[ADXL345][+] Set default data format\n");

    /* Set FIFO as samples with 20 as parameter */
    fifo_mode = STREAM<<FIFO_MODE_OFFSET | (WATERMARK_LIMIT & SAMPLES_MASK);
    adxl345_write_to_reg(client, FIFO_CTL, fifo_mode);
    pr_info("[ADXL345][+] FIFO set in samples mode (param=20)\n");

    /* Activate Measurement mode */
    adxl345_write_to_reg(client, POWER_CTL, (uint8_t)MEASURE);
    pr_info("[ADXL345][+] Measurement activated\n");

    /* Wait and test to read a value */
    pr_info("[ADXL345][+] Reading values to test\n");

    for (nbr_iter=0; nbr_iter<5; nbr_iter++) {
        /* Active waiting to avoid fast reading */
        for (i=0; i<10000000; i++) {
            useless++;
        }
        adxl345_read_data(client, xyz);
        x =  ((int16_t)xyz[1] << 8) | (int16_t)xyz[0];
        y =  ((int16_t)xyz[3] << 8) | (int16_t)xyz[2];
        z =  ((int16_t)xyz[5] << 8) | (int16_t)xyz[4];
        pr_info("\t\t#%d Reading: x: %d, y:%d, z:%d\n",nbr_iter,  x, y, z);
    }

    /* Register to the misc framework */
    misc_register(&this_device->miscdev);
    pr_info("[ADXL345][+] MISC registered\n");

    /* Return everything was fine */
    pr_info("[ADXL345][+] Initialisation done\n");
    return 0;
}

static int adxl345_remove(struct i2c_client *client)
{

    /* variables declarations */
    uint8_t POWER_CTL_value = 0;
    struct adxl345_device *this_device;


    /* Shut down the accelerometer */
    adxl345_read_from_reg(client, POWER_CTL, &POWER_CTL_value);
    POWER_CTL_value &= 0b11110111 ;
    adxl345_write_to_reg(client, POWER_CTL, POWER_CTL_value);
    pr_info("[ADXL345][+] Measurement activated\n");

    this_device = (struct adxl345_device *) i2c_get_clientdata(client);

    /* Free wait queue */
    kfree(this_device->queue);

    /* De register from the misc framework */
    misc_deregister(&this_device->miscdev);
    kfree(this_device);



    /* Notify the function is called */
    pr_info("[ADXL345][+] I2C Module unloaded\n");

    /* Return everything was fine */
    return 0;
}

/* La liste suivante permet l'association entre un périphérique et son
   pilote dans le cas d'une initialisation statique sans utilisation de
   device tree.

   Chaque entrée contient une chaîne de caractère utilisée pour
   faire l'association et un entier qui peut être utilisé par le
   pilote pour effectuer des traitements différents en fonction
   du périphérique physique détecté (cas d'un pilote pouvant gérer
   différents modèles de périphérique).
*/
static struct i2c_device_id adxl345_idtable[] = {
    { "adxl345", 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, adxl345_idtable);

#ifdef CONFIG_OF
/* Si le support des device trees est disponible, la liste suivante
   permet de faire l'association à l'aide du device tree.

   Chaque entrée contient une structure de type of_device_id. Le champ
   compatible est une chaîne qui est utilisée pour faire l'association
   avec les champs compatible dans le device tree. Le champ data est
   un pointeur void* qui peut être utilisé par le pilote pour
   effectuer des traitements différents en fonction du périphérique
   physique détecté.
*/
static const struct of_device_id adxl345_of_match[] = {
    { .compatible = "qemu,adxl345",
      .data = NULL },
    {}
};

MODULE_DEVICE_TABLE(of, adxl345_of_match);
#endif

static struct i2c_driver adxl345_driver = {
    .driver = {
        /* Le champ name doit correspondre au nom du module
           et ne doit pas contenir d'espace */
        .name   = "adxl345",
        .of_match_table = of_match_ptr(adxl345_of_match),
    },

    .id_table       = adxl345_idtable,
    .probe          = adxl345_probe,
    .remove         = adxl345_remove,
};

module_i2c_driver(adxl345_driver);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("adxl345 driver");
MODULE_AUTHOR("Jules Vranckx jules.vranckx@telecom-paris.fr");